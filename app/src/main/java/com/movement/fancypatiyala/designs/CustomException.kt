package com.movement.fancypatiyala.designs

class CustomException(message: String) : Exception(message)
